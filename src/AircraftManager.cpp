//
// Created by LoïcChapron on 07/03/2022.
//

#include "AircraftManager.hpp"
#include <algorithm>
#include <iostream>
#include <string>

void AircraftManager::move()
{
    /*for (auto it = aircrafts.begin(); it != aircrafts.end();) {
        (*it)->move();
        if ((*it)->deleted()) {
            it = aircrafts.erase(it);
        }
        else {
            it++;
        }
    }*/
    std::sort(
        aircrafts.begin(),
        aircrafts.end(),
        [](std::unique_ptr<Aircraft>& ac1, std::unique_ptr<Aircraft>& ac2){
            if (ac1->has_terminal() && !ac2->has_terminal()) return true;
            if (ac2->has_terminal() && !ac1->has_terminal()) return false;
            return ac1->get_fuel() < ac2->get_fuel();
        }
    );
    aircrafts.erase(
        std::remove_if(
            aircrafts.begin(),
            aircrafts.end(),
            [this](auto& aircraft){
                try
                {
                    aircraft->move();
                } catch (AircraftCrash& crash) {
                    std::cout << crash.what() << std::endl;
                    nb_aircraft_crash++;
                    return true;
                }
                return aircraft->deleted();
            }
        ),
        aircrafts.end()
    );
}

void AircraftManager::get_number_aircraft(int nb_airlines) {
    assert(nb_airlines >= 0 && nb_airlines < 8);
    auto str = aircraft_factory.get_airlines(nb_airlines);
    auto count = std::count_if(
        aircrafts.begin(),
        aircrafts.end(),
        [&str](auto& aircraft){return aircraft->get_flight_num().find(str) != std::string::npos;});
    std::cout << "Number of Aircraft of line " << str << " is " << count << std::endl;
}

void AircraftManager::add_aircraft(Airport* airport)
{
    assert(airport);
    auto aircraft = aircraft_factory.create_random_aircraft(airport);
    aircrafts.emplace_back(std::move(aircraft));
}

int AircraftManager::get_required_fuel() {
    return std::accumulate(
        aircrafts.begin(),
        aircrafts.end(),
        0,
        [](int accumulate, std::unique_ptr<Aircraft>& e){
            if (e->is_low_on_fuel() && !e->served())
                return accumulate + DEFAULT_MAX_FUEL - e->get_fuel();
            return accumulate;
        }
    );
}

int AircraftManager::get_number_crash() const {
    return nb_aircraft_crash;
}