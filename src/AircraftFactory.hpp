//
// Created by LoïcChapron on 21/03/2022.
//

#ifndef TOWER_SIM_AIRCRAFTFACTORY_H
#define TOWER_SIM_AIRCRAFTFACTORY_H

constexpr size_t NUM_AIRCRAFT_TYPES = 3;

class AircraftFactory
{
private:
    std::string airlines[8] = { "AF", "LH", "EY", "DL", "KL", "BA", "AY", "EY" };
    AircraftType* aircraft_types[NUM_AIRCRAFT_TYPES] {};
    std::unordered_set<std::string> aircraft_name;

public:
    AircraftFactory()
    {
        aircraft_types[0] = new AircraftType { .02f, .05f, .02f, MediaPath { "l1011_48px.png" } };
        aircraft_types[1] = new AircraftType { .02f, .05f, .02f, MediaPath { "b707_jat.png" } };
        aircraft_types[2] = new AircraftType { .03f, .08f, .02f, MediaPath { "concorde_af.png" } };
    }
    std::unique_ptr<Aircraft> create_aircraft(const AircraftType& type, Airport* airport);
    std::unique_ptr<Aircraft> create_random_aircraft(Airport* airport);
    const std::string get_airlines(int nb){ return airlines[nb]; }
};

#endif // TOWER_SIM_AIRCRAFTFACTORY_H