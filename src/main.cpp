#include "tower_sim.hpp"

void print_point2D(Point2D point) {
    std::cout << point.x() << " " << point.y() << std::endl;
}

void print_point3D(Point3D point) {
    std::cout << point.x() << " " << point.y() << " " << point.z() << std::endl;
}

void test_generic_points() {
    Point3D p1 {1, 2, 3};
    Point3D p2 {4, 5, 6};
    auto p3 = p1 + p2;
    print_point3D(p3);
    p1 += p2;
    p1 *= 3;
    print_point3D(p1);
    //Point2D p4 {1, 2, 3}; //Pb car il va utilisé le constructeur a 3 parametre mais la liste d'élément ne peut prendre que deux seulement
    //Point3D p5 {1, 2}; // marche car il appel le constructeur de Point2D // marche plus avec l'assert
    Point3D p5 {1, 2, 4};
    p1 += p5;
    print_point3D(p1);
}


int main(int argc, char** argv)
{
    //test_generic_points();
    TowerSimulation simulation { argc, argv };
    simulation.launch();

    return 0;
}