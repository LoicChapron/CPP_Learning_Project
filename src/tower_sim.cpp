#include "tower_sim.hpp"

#include "GL/opengl_interface.hpp"
#include "aircraft.hpp"
#include "airport.hpp"
#include "config.hpp"
#include "img/image.hpp"
#include "img/media_path.hpp"

#include <cassert>
#include <cstdlib>
#include <ctime>

using namespace std::string_literals;

TowerSimulation::TowerSimulation(int argc, char** argv) :
    help { (argc > 1) && (std::string { argv[1] } == "--help"s || std::string { argv[1] } == "-h"s) },
    context_initializer {ContextInitializer(argc, argv) }
{
    GL::move_queue.emplace(&aircraft_manager);

    create_keystrokes();
}

TowerSimulation::~TowerSimulation()
{
    delete airport;
}

void TowerSimulation::create_keystrokes()
{
    GL::keystrokes.emplace('x', []() { GL::exit_loop(); });
    GL::keystrokes.emplace('q', []() { GL::exit_loop(); });
    GL::keystrokes.emplace('c', [this]() { aircraft_manager.add_aircraft(airport); });
    GL::keystrokes.emplace('+', []() { GL::change_zoom(0.95f); });
    GL::keystrokes.emplace('-', []() { GL::change_zoom(1.05f); });
    GL::keystrokes.emplace('f', []() { GL::toggle_fullscreen(); });
    GL::keystrokes.emplace('m', []() { GL::ticks_per_sec++; });
    GL::keystrokes.emplace('l', []() { GL::ticks_per_sec--; });
    GL::keystrokes.emplace('p', []() { GL::pause = !GL::pause; GL::timer(0); });
    GL::keystrokes.emplace('0', [this]() { aircraft_manager.get_number_aircraft(0); });
    GL::keystrokes.emplace('1', [this]() { aircraft_manager.get_number_aircraft(1); });
    GL::keystrokes.emplace('2', [this]() { aircraft_manager.get_number_aircraft(2); });
    GL::keystrokes.emplace('3', [this]() { aircraft_manager.get_number_aircraft(3); });
    GL::keystrokes.emplace('4', [this]() { aircraft_manager.get_number_aircraft(4); });
    GL::keystrokes.emplace('5', [this]() { aircraft_manager.get_number_aircraft(5); });
    GL::keystrokes.emplace('6', [this]() { aircraft_manager.get_number_aircraft(6); });
    GL::keystrokes.emplace('7', [this]() { aircraft_manager.get_number_aircraft(7); });
    GL::keystrokes.emplace('g', [this]() { std::cout << "Number of crashed aircraft : " << aircraft_manager.get_number_crash() << std::endl; });
}

void TowerSimulation::display_help() const
{
    std::cout << "This is an airport tower simulator" << std::endl
              << "the following keysstrokes have meaning:" << std::endl;

    for (const auto& [key, value] : GL::keystrokes)
    {
        std::cout << key << ' ';
    }

    std::cout << std::endl;
}

void TowerSimulation::init_airport()
{
    airport = new Airport { one_lane_airport, Point3D { 0, 0, 0 },
                            new img::Image { one_lane_airport_sprite_path.get_full_path() }, aircraft_manager };

    GL::display_queue.emplace_back(airport);
    GL::move_queue.emplace(airport);
}

void TowerSimulation::launch()
{
    if (help)
    {
        display_help();
        return;
    }

    init_airport();


    GL::loop();
}
