//
// Created by LoïcChapron on 21/03/2022.
//

#include "GL/opengl_interface.hpp"
#include "aircraft.hpp"
#include "airport.hpp"
#include "img/media_path.hpp"

#include <cassert>
#include <cstdlib>

#include "AircraftFactory.hpp"

using namespace std::string_literals;

std::unique_ptr<Aircraft> AircraftFactory::create_aircraft(const AircraftType& type, Airport* airport)
{
    assert(airport); // make sure the airport is initialized before creating aircraft

    std::string flight_number = airlines[std::rand() % 8] + std::to_string(1000 + (rand() % 9000));
    while (aircraft_name.find(flight_number) != aircraft_name.end()) {
        flight_number = airlines[std::rand() % 8] + std::to_string(1000 + (rand() % 9000));
    }
    aircraft_name.insert(flight_number);

    const float angle       = (rand() % 1000) * 2 * 3.141592f / 1000.f; // random angle between 0 and 2pi
    const Point3D start     = Point3D { std::sin(angle), std::cos(angle), 0 } * 3 + Point3D { 0, 0, 2 };
    const Point3D direction = (-start).normalize();

    return std::make_unique<Aircraft>(type, flight_number, start, direction, airport->get_tower());
}



std::unique_ptr<Aircraft> AircraftFactory::create_random_aircraft(Airport* airport)
{
    assert(airport);
    return create_aircraft(*(aircraft_types[rand() % 3]), airport);
}
