//
// Created by LoïcChapron on 07/03/2022.
//
#pragma once

#include <vector>
#include "aircraft.hpp"
#include "AircraftFactory.hpp"

#ifndef TOWER_SIM_AIRCRAFTMANAGER_HPP
#define TOWER_SIM_AIRCRAFTMANAGER_HPP

class AircraftManager : public GL::DynamicObject
{
private :
    std::vector<std::unique_ptr<Aircraft>> aircrafts;
    AircraftFactory aircraft_factory;
    int nb_aircraft_crash = 0;

public :

    void move() override;

    void get_number_aircraft(int nb_airlines);

    void add_aircraft(Airport* airport);

    int get_required_fuel();

    int get_number_crash() const;

    ~AircraftManager() {}

};

#endif // TOWER_SIM_AIRCRAFTMANAGER_HPP
